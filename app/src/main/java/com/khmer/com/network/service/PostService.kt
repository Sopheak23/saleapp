package com.khmer.com.network.service

import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Query

interface PostService {
    @GET("api/post")
    fun getPost(@Header("Authorization") token: String, @Query("page") page: String, @Query("limit") limit: String): Call<ResponseBody>


}