package com.khmer.com.network

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class ApiHelper {

    var SHOW_LOG = true


    private val DEFAULT_TIMEOUT: Long = 30
    val BASE_URL = "https://khapi.richkidsmedia.com/"
    var mOkHttpClient: OkHttpClient? = null
    var mRetrofit: Retrofit? = null

    companion object {
        @Volatile
        var sApiCreate: ApiHelper? = null

        private fun initApiCreate(): ApiHelper {
            if (sApiCreate == null) {
                synchronized(ApiHelper::class.java) {
                    if (sApiCreate == null) {
                        sApiCreate = ApiHelper()
                    }
                }
            }
            return sApiCreate!!
        }

        val newInstance: Retrofit
            get() = initApiCreate().mRetrofit!!

    }

    init {
        // Create the logging interceptor
        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY


        mOkHttpClient = OkHttpClient.Builder()
                .connectTimeout(DEFAULT_TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(DEFAULT_TIMEOUT, TimeUnit.SECONDS)
//                .addNetworkInterceptor(networkCacheInterceptor)
//                .cache(cache) //Cache
                .addInterceptor(loggingInterceptor) //Cache
                .build()

        mRetrofit = Retrofit.Builder()
                .client(mOkHttpClient)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BASE_URL)
                .build()
    }

}
