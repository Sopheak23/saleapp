package com.khmer.com.network.service

import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.Header
import retrofit2.http.POST

interface AccountService {

    @FormUrlEncoded
    @POST("api/auth/register")
    fun register(@Field("name") name: String, @Field("email") email: String, @Field("password") password: String, @Field("profile") base64Image: String): Call<ResponseBody>

    @FormUrlEncoded
    @POST("api/auth/login")
    fun login(@Field("email") email: String, @Field("password") password: String): Call<ResponseBody>

    @POST("api/auth/logout")
    fun logout(@Header("Authorization") token: String): Call<ResponseBody>

}