package com.millionaha.utils.extensions

import android.content.res.ColorStateList
import android.net.Uri
import android.os.Build
import android.support.annotation.ColorInt
import android.support.annotation.IdRes
import android.support.annotation.StyleRes
import android.support.design.widget.Snackbar
import android.support.design.widget.TextInputEditText
import android.support.design.widget.TextInputLayout
import android.support.v4.widget.TextViewCompat
import android.text.Html
import android.text.TextUtils
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import timber.log.Timber

fun View.visible(){
    this.visibility = View.VISIBLE
}
fun View.visible(visible : Boolean){
    if(visible){
        visible()
    }else{
        gone()
    }
}
fun View.gone(){
    this.visibility = View.GONE
}
fun View.invisible(){
    this.visibility = View.INVISIBLE
}

fun View.isVisible(): Boolean{
    return visibility == View.VISIBLE
}
fun View.isGone(): Boolean{
    return visibility == View.GONE
}
fun View.isInvisible(): Boolean{
    return visibility == View.INVISIBLE
}

fun View.snackbar(resId: Int, duration: Int = Snackbar.LENGTH_SHORT) {
    snackbar(this.resources.getString(resId), duration)
}

fun View.snackbar(msg: String, duration: Int = Snackbar.LENGTH_SHORT) {
    Snackbar.make(this, msg, duration).show()
}
fun View.longSnackbar(resId: Int) {
    snackbar(resId, Snackbar.LENGTH_LONG)
}


fun TextInputEditText.setTextEx(text: CharSequence?){
    this.setText(text)
/*
    if(text?.toString()?.isEmpty() == true){
        setInputTextLayoutColor(Color.RED)
    }else{
        setInputTextLayoutColor(Color.LTGRAY)
    }
    */
}
fun TextInputEditText.setInputTextLayoutColor(@ColorInt color: Int) {
    try {

        val layout = if(parent is FrameLayout){
            parent.parent as TextInputLayout
        }else{
            parent as TextInputLayout
        }

        layout.editText?.highlightColor = color
        layout.editText?.setHintTextColor(color)
        layout.editText?.setTextColor(color)

        val fDefaultTextColor = TextInputLayout::class.java.getDeclaredField("mDefaultTextColor")
        fDefaultTextColor.isAccessible = true
        fDefaultTextColor.set(layout, ColorStateList(arrayOf(intArrayOf(0)), intArrayOf(color)))

        val fFocusedTextColor = TextInputLayout::class.java.getDeclaredField("mFocusedTextColor")
        fFocusedTextColor.isAccessible = true
        fFocusedTextColor.set(layout, ColorStateList(arrayOf(intArrayOf(0)), intArrayOf(color)))
    } catch (e: Exception) {
        Timber.e(e)
    }

}



fun TextView.setTextAppearanceC(@StyleRes textAppearance: Int)
    = TextViewCompat.setTextAppearance(this, textAppearance)

@Suppress("DEPRECATION")
fun String?.fromHtml() = (if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
    Html.fromHtml(this, Html.FROM_HTML_MODE_COMPACT);
} else {
    Html.fromHtml(this)
})!!

fun <T : View> View.bind(@IdRes res : Int) : Lazy<T> {
    @Suppress("UNCHECKED_CAST")
    return lazy(LazyThreadSafetyMode.NONE){ findViewById<T>(res) }
}

fun ImageView.loadImgRounded(imageUrl: String) {
    val imageUri = Uri.parse(imageUrl)
    if (TextUtils.isEmpty(imageUri.toString())) {
        Glide.with(this)
                .load(imageUri)
                .into(this)
    } else {
        Glide.with(this)
                .load(imageUri)
                .apply(RequestOptions.circleCropTransform())
                .into(this)
    }
}
fun <T> androidLazy(initializer: () -> T) : Lazy<T> = lazy(LazyThreadSafetyMode.NONE, initializer)

fun View.setMargins(
        left: Int? = null,
        top: Int? = null,
        right: Int? = null,
        bottom: Int? = null
) {
    val lp = layoutParams as? ViewGroup.MarginLayoutParams
            ?: return

    lp.setMargins(
            left ?: lp.leftMargin,
            top ?: lp.topMargin,
            right ?: lp.rightMargin,
            bottom ?: lp.rightMargin
    )

    layoutParams = lp
}

