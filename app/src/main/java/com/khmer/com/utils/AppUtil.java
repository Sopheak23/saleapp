package com.khmer.com.utils;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.util.Base64;
import android.util.Log;

import com.khmer.com.R;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AppUtil {

    public static void showLog(String tag, String text) {
        if (true) {
            Log.d(tag, text);
        }
    }

    /**
     * Get the network info
     *
     * @param context
     * @return
     */
    public static NetworkInfo getNetworkInfo(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo();
    }

    /**
     * Check if there is any connectivity
     *
     * @param context
     * @return
     */
    public static boolean isConnected(Context context) {
        NetworkInfo info = AppUtil.getNetworkInfo(context);
        return (info != null && info.isConnected());
    }


    //Encode ImageUri to Bitmap
    public String encodeImageUri(Activity activity, Uri uri) {
        InputStream imageStream;
        try {
            imageStream = activity.getContentResolver().openInputStream(uri);
            Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
            String encodedImage = encodeImage(selectedImage);
            return encodedImage;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return e.getMessage();
        }

    }

    //Encode Bitmap in base64
    public String encodeImage(Bitmap bm) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] b = baos.toByteArray();
        String encImage = Base64.encodeToString(b, Base64.DEFAULT);
        return encImage;
    }

    //Encode from FilePath to base64
    public String encodeImage(String path) {
        File imagefile = new File(path);
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(imagefile);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        Bitmap bm = BitmapFactory.decodeStream(fis);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] b = baos.toByteArray();
        String encImage = Base64.encodeToString(b, Base64.DEFAULT);
        //Base64.de
        return encImage;

    }

    //Verify Email
    public static boolean validateEmail(String email) {
        String reg = "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$";
        Pattern pattern = Pattern.compile(reg, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    //Username
    public static boolean validateUserName(String name) {
//        String reg = "^[a-zA-Z_0-9-]{3,10}$";
//        Pattern pattern = Pattern.compile(reg);
//        Matcher matcher = pattern.matcher(name);
//        return matcher.matches();
        if (name.length() >= 4)
            return true;
        else
            return false;
    }

    public static boolean validatePassword(String name) {
        if (name.length() >= 6)
            return true;
        else
            return false;
    }

    public String covertMinToDate(Context mContext, long totalMin) {
        long day = 0;
        long hour = 0;
        long min = 0;
        long week = 0;
        String dateTime = "";

        week = (totalMin / 1440) / 7;
        day = (totalMin / 1440) % 7;
        hour = (totalMin % 1440) / 60;
        min = ((totalMin % 1440) % 60);

        if (week > 0) {
            long year = week / 52;
            if (year > 0) {
                dateTime = year + mContext.getResources().getString(R.string.year);
            } else {
                if (day > 0) {
                    dateTime = week + mContext.getResources().getString(R.string.week) + " " + day + mContext.getResources().getString(R.string.day);

                } else {
                    dateTime = week + mContext.getResources().getString(R.string.week);

                }
            }
        } else if (day > 0) {
            dateTime = day + mContext.getResources().getString(R.string.day) + " " + hour + "" + mContext.getResources().getString(R.string.hour) + " " + min + "" + mContext.getResources().getString(R.string.minute);
        } else if (day == 0 && hour > 0) {
            dateTime = hour + "" + mContext.getResources().getString(R.string.hour) + " " + min + "" + mContext.getResources().getString(R.string.minute);
        } else if (day == 0 && hour == 0 && min > 0) {
            dateTime = min + " " + mContext.getResources().getString(R.string.minute);
        }
        return dateTime;
    }

    public static String convertSimpleDate(String date) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ssZ");
        Date result;
        String dateResult = "";
        try {

            result = df.parse(date);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
            dateResult = sdf.format(result);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dateResult;
    }

    public long printDifferenceMinute(String sDate) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ssZ");
        Date startDate = null;
        try {
            startDate = df.parse(sDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Date endDate = Calendar.getInstance().getTime();


        long different = 0;
        long resultDifferent = 0;
        different = endDate.getTime() - startDate.getTime();

        System.out.println("startDate : " + startDate);
        System.out.println("endDate : " + endDate);
        System.out.println("different : " + different);
        long seconds = different / 1000;
        long minutes = seconds / 60;
        resultDifferent = minutes;
        return resultDifferent;
    }

    public void printDifference(Date startDate, Date endDate) {
        //milliseconds
        long different = endDate.getTime() - startDate.getTime();

        System.out.println("startDate : " + startDate);
        System.out.println("endDate : " + endDate);
        System.out.println("different : " + different);

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = different / daysInMilli;
        different = different % daysInMilli;

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        long elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;

        long elapsedSeconds = different / secondsInMilli;

        System.out.printf(
                "%d days, %d hours, %d minutes, %d seconds%n",
                elapsedDays, elapsedHours, elapsedMinutes, elapsedSeconds);
    }

}
