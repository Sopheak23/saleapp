package com.khmer.com.utils

interface OnResponseListener {
     fun onResponded(isSuccess: Boolean)
}