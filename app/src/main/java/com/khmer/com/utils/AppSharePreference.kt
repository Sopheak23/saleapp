package com.khmer.com.utils

import android.content.Context
import android.content.SharedPreferences
import android.support.annotation.Nullable
import com.khmer.com.app.KJApplication
import com.khmer.com.base.APP_PREF_FILE_NAME

class AppSharePreference {
    companion object {
        fun putInt(key: String, value: Int) {
            val preferences: SharedPreferences = com.khmer.com.app.KJApplication.instance
                    .getSharedPreferences(APP_PREF_FILE_NAME, Context.MODE_PRIVATE)
            val editor: SharedPreferences.Editor = preferences.edit()
            editor.putInt(key, value)
            editor.apply()
        }

        fun getInt(key: String, default: Int): Int {
            val preferences: SharedPreferences = com.khmer.com.app.KJApplication.instance
                    .getSharedPreferences(APP_PREF_FILE_NAME, Context.MODE_PRIVATE)
            return preferences.getInt(key, default)
        }

        fun putString(key: String, value: String?) {
            val preferences: SharedPreferences =  com.khmer.com.app.KJApplication.instance
                    .getSharedPreferences(APP_PREF_FILE_NAME, Context.MODE_PRIVATE)
            val editor: SharedPreferences.Editor = preferences.edit()
            editor.putString(key, value)
            editor.apply()
        }

        fun getString(key: String, @Nullable default: String?): String {
            val preferences: SharedPreferences =  com.khmer.com.app.KJApplication.instance
                    .getSharedPreferences(APP_PREF_FILE_NAME, Context.MODE_PRIVATE)
            return preferences.getString(key, default)
        }

        fun putBoolean(key: String, value: Boolean) {
            val preferences: SharedPreferences =  com.khmer.com.app.KJApplication.instance
                    .getSharedPreferences(APP_PREF_FILE_NAME, Context.MODE_PRIVATE)
            val editor: SharedPreferences.Editor = preferences.edit()
            editor.putBoolean(key, value)
            editor.apply()
        }

        fun getBoolean(key: String, default: Boolean): Boolean {
            val preferences: SharedPreferences =  com.khmer.com.app.KJApplication.instance
                    .getSharedPreferences(APP_PREF_FILE_NAME, Context.MODE_PRIVATE)
            return preferences.getBoolean(key, default)
        }

        fun putLong(key: String, value: Long) {
            val preferences: SharedPreferences =  com.khmer.com.app.KJApplication.instance
                    .getSharedPreferences(APP_PREF_FILE_NAME, Context.MODE_PRIVATE)
            val editor: SharedPreferences.Editor = preferences.edit()
            editor.putLong(key, value)
            editor.apply()
        }

        fun getLong(key: String, default: Long): Long {
            val preferences: SharedPreferences =  com.khmer.com.app.KJApplication.instance
                    .getSharedPreferences(APP_PREF_FILE_NAME, Context.MODE_PRIVATE)
            return preferences.getLong(key, default)
        }

        fun clear() {
            com.khmer.com.app.KJApplication.instance.getSharedPreferences(APP_PREF_FILE_NAME, Context.MODE_PRIVATE).edit().clear().apply()
        }
    }
}