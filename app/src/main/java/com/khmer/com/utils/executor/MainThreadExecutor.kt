package com.millionaha.aha.utils.executor

import android.os.Looper
import java.util.concurrent.Executor
import android.os.Handler

class MainThreadExecutor: Executor {
    override fun execute(command: Runnable?) {
        mainThreadExecutor.post(command)
    }

    val mainThreadExecutor = Handler(Looper.getMainLooper())


}