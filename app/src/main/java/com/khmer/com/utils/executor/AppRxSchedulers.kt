package com.millionaha.aha.utils.executor

import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

class AppRxSchedulers : RxSchedulers {

    override fun runOnBackground(): Scheduler? {
        return BACKGROUND_SCHEDULERS
    }

    override fun io(): Scheduler = Schedulers.io()

    override fun compute(): Scheduler = Schedulers.computation()

    override fun androidThread(): io.reactivex.Scheduler = AndroidSchedulers.mainThread()

    override fun internet(): Scheduler? = INTERNET_SCHEDULERS

    companion object {
        private var backgroundExecutor: ExecutorService? = Executors.newCachedThreadPool()
        private var internetExecutor: ExecutorService? = Executors.newCachedThreadPool()
        var BACKGROUND_SCHEDULERS = backgroundExecutor?.let { Schedulers.from(it) }
        var INTERNET_SCHEDULERS = internetExecutor?.let { Schedulers.from(it) }
    }

}