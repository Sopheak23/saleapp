package com.millionaha.aha.utils.executor

import java.util.concurrent.Executor
import javax.inject.Singleton

@Singleton
class AppExecutor(private val diskIO: Executor, private val mainThread: Executor) {

    fun diskIO(): Executor {
        return diskIO
    }

    fun mainThread(): Executor {
        return mainThread
    }
}