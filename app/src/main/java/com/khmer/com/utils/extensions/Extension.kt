package com.millionaha.utils.extensions

import android.app.Activity
import android.content.res.Resources
import android.support.annotation.ColorRes
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.view.View
import android.widget.Toast


fun Activity.getCompactColor(@ColorRes colorRes: Int): Int = ContextCompat.getColor(this, colorRes)

fun Fragment.showToast(content: String): Toast {
  val toast = Toast.makeText(this.activity?.applicationContext, content, Toast.LENGTH_SHORT)
  toast.show()
  return toast
}

fun Activity.showToast(content: String): Toast {
  val toast = Toast.makeText(applicationContext, content, Toast.LENGTH_SHORT)
  toast.show()
  return toast
}

fun View.dip2px(dipValue: Float): Int {
  val scale = this.resources.displayMetrics.density
  return (dipValue * scale + 0.5f).toInt()
}

fun View.px2dip(pxValue: Float): Int {
  val scale = this.resources.displayMetrics.density
  return (pxValue / scale + 0.5f).toInt()
}



fun Activity.phoneFormat(data: String) : String {
  val value = data.trim()
  val zero = value[0].toInt()
  return when { value[0].toInt() != 48 -> { val c = "0" + data.trim()
      c
    } else -> value }
}

val Int.dp: Int
  get() = (this / Resources.getSystem().displayMetrics.density).toInt()
val Int.px: Int
  get() = (this * Resources.getSystem().displayMetrics.density).toInt()