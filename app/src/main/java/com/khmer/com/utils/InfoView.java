package com.khmer.com.utils;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.khmer.com.R;

public class InfoView extends RelativeLayout {

    //Views
    private RelativeLayout mLayout;
    private ProgressBar mProgressBar;
    private ProgressBar progressBarWhite;
    private TextView txtInfo;

    //Objects
    private Context mContext;

    public InfoView(Context context) {
        super(context);
        init();
    }

    public InfoView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public InfoView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        inflate(getContext(), R.layout.layout_info_view, this);
        mLayout = findViewById(R.id.layoutInfo);
        mProgressBar = findViewById(R.id.progressBar);
        txtInfo = findViewById(R.id.txtInfo);
    }

    public void showLoading() {
        mLayout.setVisibility(VISIBLE);
        mLayout.setClickable(true);
        mLayout.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.Transparent));
        mProgressBar.setVisibility(VISIBLE);
        txtInfo.setVisibility(GONE);
    }

    public void showInfo(String message) {
        mLayout.setVisibility(VISIBLE);
        mLayout.setClickable(false);
        mLayout.setBackgroundColor(Color.WHITE);
        mProgressBar.setVisibility(GONE);
        txtInfo.setVisibility(VISIBLE);
        txtInfo.setText(message);
    }

    public void hide() {
        mLayout.setVisibility(GONE);
    }

    public TextView getTxtInfo() {
        return txtInfo;
    }
}