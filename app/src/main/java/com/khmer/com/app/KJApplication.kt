package com.khmer.com.app

import android.app.Application
import com.sophal.homefinder.di.component.AppComponent
import com.sophal.homefinder.di.component.DaggerAppComponent
import com.sophal.homefinder.di.module.AppModule

class KJApplication : Application() {

    private lateinit var appComponent: AppComponent

    companion object {
        lateinit var instance: KJApplication
            private set
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
        initDagger()
    }

    fun getAppComponent(): AppComponent {
        return appComponent
    }


    private fun initDagger() {
        appComponent = DaggerAppComponent
                .builder()
                .appModule(AppModule(this))
                .build()

    }

}