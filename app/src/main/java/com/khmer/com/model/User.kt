package com.khmer.com.model

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class User(

        @field:SerializedName("profile")
        var profile: Profile? = null,

        @field:SerializedName("name")
        var name: String? = null,

        @field:SerializedName("_token")
        var token: String? = null,

        @field:SerializedName("id")
        var id: String? = null,

        @field:SerializedName("email")
        var email: String? = null,

        @field:SerializedName("status")
        var status: String? = null
)