package com.khmer.com.model

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class Profile(

	@field:SerializedName("thumbnail")
	var thumbnail: String? = null,

	@field:SerializedName("file_name")
	var fileName: String? = null
)