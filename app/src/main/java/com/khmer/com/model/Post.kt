package com.khmer.com.model

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class Post(

	@field:SerializedName("data")
	var data: List<DataItem> = listOf(),

	@field:SerializedName("total_pages")
	var totalPages: String? = null,

	@field:SerializedName("current_page")
	var currentPage: String? = null,

	@field:SerializedName("status")
	var status: String? = null
)