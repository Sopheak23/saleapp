package com.khmer.com.model

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class TranslateItem(

	@field:SerializedName("post_detail")
	var postDetail: String? = null,

	@field:SerializedName("caption")
	var caption: String? = null,

	@field:SerializedName("language")
	var language: String? = null
)