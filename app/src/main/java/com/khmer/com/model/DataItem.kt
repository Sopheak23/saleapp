package com.khmer.com.model

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class DataItem(

	@field:SerializedName("images")
	var images: List<ImagesItem?>? = null,

	@field:SerializedName("company_logo")
	var companyLogo: String? = null,

	@field:SerializedName("company_name")
	var companyName: String? = null,

	@field:SerializedName("created_at")
	var createdAt: String? = null,

	@field:SerializedName("id")
	var id: String? = null,

	@field:SerializedName("translate")
	var translate: List<TranslateItem?>? = null
)