package com.khmer.com.ui.viewholder

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.khmer.com.R
import kotlinx.android.synthetic.main.cell_progress_bar.view.*

class NoDataViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    val tvNoData = view.tv_no_data

    companion object {
        fun create(parent: ViewGroup): PostViewHolder {

            val layoutInflater = LayoutInflater.from(parent.context)
            val view = layoutInflater.inflate(R.layout.cell_no_data, parent, false)
            return PostViewHolder(view)
        }
    }

}