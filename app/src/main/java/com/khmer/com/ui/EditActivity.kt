package com.khmer.com.ui

import android.app.Activity
import android.arch.lifecycle.ViewModel
import android.content.Intent
import android.os.Bundle
import android.os.SystemClock
import android.support.v7.app.AlertDialog
import android.widget.Toast
import com.bumptech.glide.Glide
import com.khmer.com.R
import com.khmer.com.app.KJApplication
import com.khmer.com.base.*
import com.khmer.com.data.datamanager.AccountManager
import com.khmer.com.utils.AppSharePreference
import com.khmer.com.utils.AppUtil
import com.khmer.com.utils.OnResponseListener
import kotlinx.android.synthetic.main.activity_edit.*
import javax.inject.Inject

class EditActivity : com.khmer.com.base.BaseActivity<ViewModel>() {

    @Inject
    lateinit var accountManager: AccountManager
    private lateinit var alertDialog: AlertDialog
    private lateinit var alertLogoutDialog: AlertDialog
    private lateinit var username: String
    var mLastClickTime: Long = 0


    override fun layoutResId(): Int {
        return R.layout.activity_edit
    }

    override fun onInitView(savedInstanceState: Bundle?) {
        (application as com.khmer.com.app.KJApplication).getAppComponent().inject(this)
        initGUI()
        initEvent()
    }

    private fun initGUI() {
        username = AppSharePreference.getString(USERNAME, "")
        val thumbnail = AppSharePreference.getString(THUMBNAIL, "")
        val email = AppSharePreference.getString(EMAIL, "")
        et_username.keyListener = null
        et_username.setText(username)
        et_email.keyListener = null
        et_email.setText(email)
        Glide.with(this).load(thumbnail).thumbnail(0.1f).into(iv_edit_image)

    }

    private fun initEvent() {
        iv_back.setOnClickListener {
            if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                return@setOnClickListener
            }
            mLastClickTime = SystemClock.elapsedRealtime()

            onBackPressed()
        }
        btn_Logout.setOnClickListener {
            if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                return@setOnClickListener
            }
            mLastClickTime = SystemClock.elapsedRealtime()

            if (com.khmer.com.utils.AppUtil.isConnected(this)) {
                showLogoutDialog()
            } else {
                Toast.makeText(this, resources.getString(R.string.no_internet), Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun gotoEditInfoActivity() {
        val intent = Intent(this, EditInfoActivity::class.java)
        startActivityForResult(intent, UPDATE_USERNAME)
        overridePendingTransition(R.anim.enter, R.anim.hold)
    }

    private fun gotoLoginActivity() {
        val intent = Intent(this, LoginActivity::class.java)
        startActivity(intent)
        overridePendingTransition(R.anim.enter, R.anim.hold)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == UPDATE_USERNAME && resultCode == Activity.RESULT_OK) {
            val username = AppSharePreference.getString(USERNAME, "")
            setResult(Activity.RESULT_OK)
            et_username.setText(username)
        }
    }

    override fun onBackPressed() {
        finish()
        overridePendingTransition(R.anim.hold, R.anim.exit)
    }

    private val onResponseListener = object : OnResponseListener {
        override fun onResponded(isSuccess: Boolean) {
            infoViewEdit.hide()
            if (isSuccess) {
                AppSharePreference.clear()
                finishAffinity()
                gotoLoginActivity()
            } else {
                showErrorDialog()
            }
        }
    }

    private fun showErrorDialog() {
        val builder = AlertDialog.Builder(this)
        builder.setCancelable(false)
        builder.setMessage(resources.getString(R.string.sth_went_wrong))
        builder.setPositiveButton(resources.getString(R.string.ok)) { dialog, which -> alertDialog.dismiss() }
        alertDialog = builder.create()
        alertDialog.show()
    }

    private fun showLogoutDialog() {
        val builder = AlertDialog.Builder(this)
        builder.setCancelable(false)
        builder.setTitle(resources.getString(R.string.hi) + " " + username)
        builder.setMessage(resources.getString(R.string.would_you_like_to_logout_now))
        builder.setPositiveButton(resources.getString(R.string.logout)) { dialog, which ->
            infoViewEdit.showLoading()
            val token = AppSharePreference.getString(TOKEN, "")
            accountManager.logout(BEARER + token, onResponseListener)
        }
        builder.setNegativeButton(resources.getString(R.string.cancel)) { dialog, which ->
            infoViewEdit.hide()
            alertLogoutDialog.dismiss()
        }
        alertLogoutDialog = builder.create()
        alertLogoutDialog.show()
    }

}
