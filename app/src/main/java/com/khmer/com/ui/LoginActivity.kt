package com.khmer.com.ui

import android.arch.lifecycle.ViewModel
import android.content.Intent
import android.os.Bundle
import android.os.SystemClock
import android.support.v7.app.AlertDialog
import android.widget.Toast
import com.khmer.com.R
import com.khmer.com.app.KJApplication
import com.khmer.com.base.*
import com.khmer.com.data.datamanager.AccountManager
import com.khmer.com.utils.AppSharePreference
import com.khmer.com.utils.AppUtil
import com.khmer.com.utils.OnResponseListener
import kotlinx.android.synthetic.main.activity_login.*
import javax.inject.Inject

class LoginActivity : com.khmer.com.base.BaseActivity<ViewModel>() {


    @Inject
    lateinit var accountManager: AccountManager

    private var email: String = ""
    private var password: String = ""
    private lateinit var alertDialog: AlertDialog
    var mLastClickTime: Long = 0


    override fun layoutResId(): Int {
        return R.layout.activity_login
    }

    override fun onInitView(savedInstanceState: Bundle?) {
        (application as com.khmer.com.app.KJApplication).getAppComponent().inject(this)
        initGUI()
        initEvent()
    }

    private fun initGUI() {
        if (AppSharePreference.getBoolean(IS_LOGIN, false)) {
            gotoMain()
        } else {
            if (AppSharePreference.getBoolean(IS_SKIP, false)) {
                gotoMain()
            }
        }
    }


    private fun initEvent() {
        tv_skip.setOnClickListener {
            if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                return@setOnClickListener
            }
            mLastClickTime = SystemClock.elapsedRealtime()

            if (com.khmer.com.utils.AppUtil.isConnected(this)) {
                AppSharePreference.putBoolean(IS_SKIP, true)
                gotoMain()
            } else {
                Toast.makeText(this, resources.getString(R.string.no_internet), Toast.LENGTH_SHORT).show()
            }
        }

        btn_login.setOnClickListener {
            if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                return@setOnClickListener
            }
            mLastClickTime = SystemClock.elapsedRealtime()

            if (com.khmer.com.utils.AppUtil.isConnected(this)) {
                infoViewLogin.showLoading()
                email = et_email.text.toString()
                password = et_password.text.toString()
                if (checkData()) {
                    accountManager.login(email, password, onResponseListener)
                }
            } else {
                Toast.makeText(this, resources.getString(R.string.no_internet), Toast.LENGTH_SHORT).show()
            }
        }

        btn_create_acc.setOnClickListener {
            if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                return@setOnClickListener
            }
            mLastClickTime = SystemClock.elapsedRealtime()

            gotoSignUp()
        }
    }

    private fun checkData(): Boolean {
        if (!com.khmer.com.utils.AppUtil.validateEmail(email)) {
            infoViewLogin.hide()
            et_email.error = resources.getText(R.string.error_email)
            return false
        }

        if (!com.khmer.com.utils.AppUtil.validatePassword(password)) {
            infoViewLogin.hide()
            et_password.error = resources.getText(R.string.error_password)
            et_password.setText("")
            return false
        }
        return true
    }

    private val onResponseListener = object : OnResponseListener {
        override fun onResponded(isSuccess: Boolean) {
            infoViewLogin.hide()
            if (isSuccess) {
                val user = accountManager.getUser()
                AppSharePreference.putBoolean(IS_LOGIN, true)
                AppSharePreference.putBoolean(IS_SKIP, false)
                AppSharePreference.putString(USERNAME, user.name)
                AppSharePreference.putString(EMAIL, user.email)
                AppSharePreference.putString(TOKEN, user.token)
                AppSharePreference.putString(THUMBNAIL, user.profile?.thumbnail)
                gotoMain()
            } else {
                showErrorDialog()
            }
        }
    }

    private fun showErrorDialog() {
        val builder = AlertDialog.Builder(this)
        builder.setCancelable(false)
        if (!accountManager.getErrorMessage().isEmpty()) {
            builder.setMessage(accountManager.getErrorMessage())
        } else {
            builder.setMessage(resources.getString(R.string.sth_went_wrong))
        }

        builder.setPositiveButton(resources.getString(R.string.ok)) { dialog, which -> alertDialog.dismiss() }
        alertDialog = builder.create()
        alertDialog.show()
    }

    private fun gotoMain() {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()
        overridePendingTransition(R.anim.enter, R.anim.hold)
    }

    private fun gotoSignUp() {
        val intent = Intent(this, SignUpActivity::class.java)
        startActivity(intent)
        finish()
        overridePendingTransition(R.anim.enter, R.anim.hold)
    }

}
