package com.khmer.com.ui

import android.arch.lifecycle.ViewModel
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.SystemClock
import android.support.v7.app.AlertDialog
import android.widget.Toast
import com.bumptech.glide.Glide
import com.theartofdev.edmodo.cropper.CropImage
import com.theartofdev.edmodo.cropper.CropImageView
import com.khmer.com.R
import com.khmer.com.app.KJApplication
import com.khmer.com.base.*


import com.khmer.com.data.datamanager.AccountManager
import com.khmer.com.utils.AppSharePreference
import com.khmer.com.utils.AppUtil
import com.khmer.com.utils.OnResponseListener
import kotlinx.android.synthetic.main.activity_sign_up.*
import javax.inject.Inject

class SignUpActivity : com.khmer.com.base.BaseActivity<ViewModel>() {

    @Inject
    lateinit var accountManager: AccountManager


    private val TAG = "SignUpActivity"
    private val appUtil = com.khmer.com.utils.AppUtil()
    private var base64: String = ""
    private var username: String = ""
    private var email: String = ""
    private var password: String = ""
    private lateinit var alertDialog: AlertDialog
    var mLastClickTime: Long = 0

    override fun layoutResId(): Int {
        return R.layout.activity_sign_up
    }

    override fun onInitView(savedInstanceState: Bundle?) {
        (application as com.khmer.com.app.KJApplication).getAppComponent().inject(this)
        initGUI()
        initEvent()
    }

    private fun initGUI() {

    }

    private fun initEvent() {
        iv_back_sign_up.setOnClickListener {
            if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                return@setOnClickListener
            }
            mLastClickTime = SystemClock.elapsedRealtime()

            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
            finish()
            overridePendingTransition(R.anim.hold, R.anim.exit)
        }

        iv_image_add.setOnClickListener {
            if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                return@setOnClickListener
            }
            mLastClickTime = SystemClock.elapsedRealtime()

            CropImage.activity()
                    .setAspectRatio(1, 1)
                    .setFixAspectRatio(true)
                    .setCropShape(CropImageView.CropShape.OVAL)
                    .setGuidelines(CropImageView.Guidelines.ON)
                    .start(this)
        }

        btn_create_acc_sign_up.setOnClickListener {
            if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                return@setOnClickListener
            }
            mLastClickTime = SystemClock.elapsedRealtime()

            if (com.khmer.com.utils.AppUtil.isConnected(this)) {
                infoView.showLoading()
                username = et_username_sign_up.text.toString()
                email = et_email_sign_up.text.toString()
                password = et_password_sign_up.text.toString()

                if (checkData()) {
                    accountManager.registerUser(username, email, password, base64, onResponseListener)
                }
            } else {
                Toast.makeText(this, resources.getString(R.string.no_internet), Toast.LENGTH_SHORT).show()
            }
        }
    }

    private val onResponseListener = object : OnResponseListener {
        override fun onResponded(isSuccess: Boolean) {
            infoView.hide()
            if (isSuccess) {
                val user = accountManager.getUser()
                AppSharePreference.putBoolean(IS_LOGIN, true)
                AppSharePreference.putBoolean(IS_SKIP, false)
                AppSharePreference.putString(USERNAME, user.name)
                AppSharePreference.putString(EMAIL, user.email)
                AppSharePreference.putString(TOKEN, user.token)
                AppSharePreference.putString(THUMBNAIL, user.profile?.thumbnail)
                gotoMain()
            } else {
                showErrorDialog()
            }
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            val result = CropImage.getActivityResult(data)
            if (resultCode == RESULT_OK) {
                val resultUri = result.uri
                loadImageProfile(resultUri)
                //                registerViewModel.imageUrl.set(mCropImageUri);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                val error = result.error
            }
        }
    }

    private fun gotoMain() {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()
        overridePendingTransition(R.anim.enter, R.anim.hold)
    }

    private fun loadImageProfile(uri: Uri) {
        Glide.with(this)
                .asBitmap()
                .load(uri)
                .thumbnail(0.1f)
                .into(iv_image)

        base64 = appUtil.encodeImageUri(this, uri)
    }

    private fun checkData(): Boolean {
        if (base64.isEmpty()) {
            infoView.hide()
            Toast.makeText(this, resources.getText(R.string.error_profile_image), Toast.LENGTH_SHORT).show()
            return false
        }

        if (!com.khmer.com.utils.AppUtil.validateUserName(username)) {
            infoView.hide()
            et_username_sign_up.error = resources.getText(R.string.error_username)
            return false
        }

        if (!com.khmer.com.utils.AppUtil.validateEmail(email)) {
            infoView.hide()
            et_email_sign_up.error = resources.getText(R.string.error_email)
            return false
        }

        if (!com.khmer.com.utils.AppUtil.validatePassword(password)) {
            infoView.hide()
            et_password_sign_up.error = resources.getText(R.string.error_password)
            return false
        }

        if (!cb_term_of_service.isChecked) {
            infoView.hide()
            Toast.makeText(this, resources.getText(R.string.error_term_of_service), Toast.LENGTH_SHORT).show()
            return false
        }
        return true
    }

    private fun showErrorDialog() {
        val builder = AlertDialog.Builder(this)
        builder.setCancelable(false)
        builder.setMessage(resources.getString(R.string.sth_went_wrong))
        builder.setPositiveButton(resources.getString(R.string.ok)) { dialog, which -> alertDialog.dismiss() }
        alertDialog = builder.create()
        alertDialog.show()
    }
}
