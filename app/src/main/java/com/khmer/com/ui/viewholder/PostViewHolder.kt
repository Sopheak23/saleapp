package com.khmer.com.ui.viewholder

import android.app.Activity
import android.content.Intent
import android.os.SystemClock
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.RequestManager
import com.bumptech.glide.request.RequestOptions
import com.khmer.com.R
import com.khmer.com.base.ENGLISH
import com.khmer.com.base.LANGUAGE
import com.khmer.com.base.URL
import com.khmer.com.model.DataItem
import com.khmer.com.ui.WebActivity
import com.khmer.com.utils.AppSharePreference
import com.khmer.com.utils.AppUtil
import kotlinx.android.synthetic.main.cell_post.view.*
import java.lang.Exception

class PostViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    val TAG = "PostViewHolder"
    val appUtil = com.khmer.com.utils.AppUtil()

    private val ivProfile = view.iv_profile
    private val ivImage = view.iv_post
    private val tvName = view.tv_name
    private val tvdate = view.tv_date
    private val tvCaption = view.tv_caption
    private val cellPost = view.cell_post
    private val language = AppSharePreference.getString(LANGUAGE, ENGLISH)
    private var languageIndex = 2
    var mLastClickTime: Long = 0

    companion object {
        fun create(parent: ViewGroup): PostViewHolder {

            val layoutInflater = LayoutInflater.from(parent.context)
            val view = layoutInflater.inflate(R.layout.cell_post, parent, false)
            return PostViewHolder(view)
        }
    }

    fun bindTo(mItemData: DataItem, glide: RequestManager) {
        tvName.text = mItemData.companyName
        val date = appUtil.printDifferenceMinute(mItemData.createdAt)
        tvdate.text = appUtil.covertMinToDate(itemView.context, date)
        try {
            languageIndex = mItemData.translate?.asSequence()?.withIndex()?.find { translate -> translate.value?.language.equals(language) }!!.index
        } catch (e: Exception) {
            com.khmer.com.utils.AppUtil.showLog(TAG, e.message)
        }
        tvCaption.text = mItemData.translate?.get(languageIndex)?.caption

        try {
            glide.load(mItemData.companyLogo)
                    .thumbnail(0.1f)
                    .into(ivProfile)
        } catch (e: Exception) {
            com.khmer.com.utils.AppUtil.showLog(TAG, e.message)
        }

        try {
            glide.load(mItemData.images?.get(0)?.thumbnail)
                    .thumbnail(0.1f)
                    .into(ivImage)
        } catch (e: Exception) {
            com.khmer.com.utils.AppUtil.showLog(TAG, e.message)
        }

        cellPost.setOnClickListener {
            if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                return@setOnClickListener
            }
            mLastClickTime = SystemClock.elapsedRealtime()

            val intent = Intent(itemView.context, WebActivity::class.java)
            intent.putExtra(URL, mItemData.translate?.get(languageIndex)?.postDetail)
            com.khmer.com.utils.AppUtil.showLog(TAG, mItemData.translate?.get(languageIndex)?.language)
            itemView.context.startActivity(intent)
            (itemView.context as Activity).overridePendingTransition(R.anim.enter, R.anim.hold)
        }
    }


    private fun imageRequestOptions(): RequestOptions {
        val requestOptions = RequestOptions()
        requestOptions.centerCrop()
        return requestOptions
    }
}