package com.khmer.com.ui.viewholder

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.khmer.com.R
import kotlinx.android.synthetic.main.cell_progress_bar.view.*

class ProgressBarViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    val progressBar = view.progressBar

    companion object {
        fun create(parent: ViewGroup): ProgressBarViewHolder {
            val layoutInflater = LayoutInflater.from(parent.context)
            val view = layoutInflater.inflate(R.layout.cell_progress_bar, parent, false)
            return ProgressBarViewHolder(view)
        }
    }

}