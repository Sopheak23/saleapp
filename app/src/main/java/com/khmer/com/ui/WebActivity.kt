package com.khmer.com.ui

import android.arch.lifecycle.ViewModel
import android.graphics.Bitmap
import android.os.Bundle
import android.webkit.WebResourceError
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import com.khmer.com.R
import com.khmer.com.base.BaseActivity
import com.khmer.com.base.URL
import kotlinx.android.synthetic.main.activity_web.*
import kotlinx.android.synthetic.main.layout_toolbar.*

class WebActivity : com.khmer.com.base.BaseActivity<ViewModel>() {

    private var url: String = ""

    override fun layoutResId(): Int {
        return R.layout.activity_web
    }

    override fun onInitView(savedInstanceState: Bundle?) {
        initGUI()
        initEvent()
    }

    private fun initGUI() {
        url = intent.getStringExtra(URL)
        webView.settings.javaScriptEnabled = true
        webView.webViewClient = object : WebViewClient() {
            override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                super.onPageStarted(view, url, favicon)
                infoViewWeb.showLoading()
            }

            override fun onReceivedError(view: WebView?, request: WebResourceRequest?, error: WebResourceError?) {
                super.onReceivedError(view, request, error)
                infoViewWeb.showInfo(error.toString())
            }

            override fun onPageFinished(view: WebView?, url: String?) {
                infoViewWeb.hide()
                tv_title_toolbar.text = view?.title
            }
        }
        webView.loadUrl(url)
    }

    private fun initEvent() {
        iv_back_toolbar.setOnClickListener {
            finish()
            overridePendingTransition(R.anim.hold, R.anim.exit)
        }

        infoViewWeb.txtInfo.setOnClickListener {
            webView.loadUrl(url)
        }
    }

    override fun onBackPressed() {
        if (webView.canGoBack()) {
            webView.goBack()
        } else {
            finish()
            overridePendingTransition(R.anim.hold, R.anim.exit)
        }
    }
}
