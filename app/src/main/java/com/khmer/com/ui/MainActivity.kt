package com.khmer.com.ui

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.SystemClock
import android.support.design.widget.BottomSheetBehavior
import android.support.design.widget.NavigationView
import android.support.v4.content.ContextCompat
import android.support.v4.view.GravityCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.Menu
import android.view.MenuItem
import android.view.View
import com.bumptech.glide.Glide
import com.millionaha.utils.extensions.invisible
import com.millionaha.utils.extensions.visible
import com.khmer.com.R
import com.khmer.com.app.KJApplication
import com.khmer.com.base.*
import com.khmer.com.data.datamanager.PostManager
import com.khmer.com.model.DataItem
import com.khmer.com.model.Post
import com.khmer.com.ui.adapter.PostAdapter
import com.khmer.com.utils.AppSharePreference
import com.khmer.com.utils.AppUtil
import com.khmer.com.utils.OnResponseListener
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.content_main.*
import kotlinx.android.synthetic.main.nav_header_main.view.*
import javax.inject.Inject
import com.millionaha.utils.extensions.gone
import kotlinx.android.synthetic.main.layout_change_caption_langauge.*


class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    @Inject
    lateinit var postManager: PostManager

    private var limit: Int = 10
    private var page: Int = 1
    private var oldPage: Int = 0
    private var isLoadData: Boolean = false
    private lateinit var headerLayout: View
    private lateinit var mPostListing: Post
    private var mDataListing: MutableList<DataItem> = mutableListOf()
    private lateinit var layoutManager: LinearLayoutManager
    private lateinit var postAdapter: PostAdapter
    private lateinit var sheetView: View
    private var mLastClickTime: Long = 0
    private var language: String = ""
    private var bottomSheetBehavior: BottomSheetBehavior<*>? = null
//    private lateinit var mBottomSheetDialog: BottomSheetDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        (application as com.khmer.com.app.KJApplication).getAppComponent().inject(this)

//        setSupportActionBar(toolbar)
//        val toggle = ActionBarDrawerToggle(
//                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
//        drawer_layout.addDrawerListener(toggle)
//        toggle.syncState()

        iv_menu.setOnClickListener {
            if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
                drawer_layout.closeDrawer(GravityCompat.START)
            } else {
                if (bottomSheetBehavior?.state == BottomSheetBehavior.STATE_EXPANDED) {
                    bottomSheetBehavior?.state = BottomSheetBehavior.STATE_COLLAPSED
                }
                drawer_layout.openDrawer(GravityCompat.START)
            }
        }
        nav_view.setNavigationItemSelectedListener(this)
        nav_view.itemIconTintList = null
        val navigationView = findViewById<View>(R.id.nav_view) as NavigationView
        headerLayout = navigationView.getHeaderView(0)

        initGUI()
        initEvent()
    }

    private fun initGUI() {
        infoViewPost.showLoading()
        language = AppSharePreference.getString(LANGUAGE, "")
        if (language.isEmpty()) {
            AppSharePreference.putString(LANGUAGE, ENGLISH)
            iv_flag.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.flag_us))
        } else {
            when (language) {
                ENGLISH -> iv_flag.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.flag_us))
                KHMER -> iv_flag.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.flag_cam))
                JAPANESE -> iv_flag.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.flag_ja))
                CHINESE -> iv_flag.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.flag_zh))
                KOREAN -> iv_flag.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.flag_ko))
                THAI -> iv_flag.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.flag_th))
            }
        }

        bottomSheetBehavior = BottomSheetBehavior.from<View>(bs_change_langauge)
//        mBottomSheetDialog = BottomSheetDialog(this)
//        sheetView = layoutInflater.inflate(R.layout.layout_change_caption_langauge, null)
//        mBottomSheetDialog.setContentView(sheetView)
        val username = AppSharePreference.getString(USERNAME,  "")
        val thumbnail = AppSharePreference.getString(THUMBNAIL, "")
        if (AppSharePreference.getBoolean(IS_LOGIN, false)) {
            headerLayout.tv_username.text = username
            Glide.with(this).load(thumbnail).thumbnail(0.1f).into(headerLayout.iv_profile)
        } else {
            headerLayout.tv_username.text = resources.getText(R.string.login_now)
        }

        getPost(page)
    }

    private fun getPost(requestPage: Int) {
        isLoadData = true
        oldPage = requestPage
        if (AppSharePreference.getBoolean(IS_LOGIN, false)) {
            val token = AppSharePreference.getString(TOKEN, "")
            postManager.getPost(BEARER + token, requestPage.toString(), limit.toString(), onResponseListener)
        } else {
            postManager.getPost("", requestPage.toString(), limit.toString(), onResponseListener)

        }

    }

    private fun initEvent() {
        val isLogin = AppSharePreference.getBoolean(IS_LOGIN, false)
        headerLayout.profile_layout.setOnClickListener {
            if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                return@setOnClickListener
            }
            mLastClickTime = SystemClock.elapsedRealtime()

            if (isLogin) {
                gotoEditActivity()
            } else {
                AppSharePreference.clear()
                gotoLogin()
            }
        }

        iv_flag.setOnClickListener {
            if (bottomSheetBehavior?.state == BottomSheetBehavior.STATE_COLLAPSED) {
                fake_bg.visible()
                bottomSheetBehavior?.setState(BottomSheetBehavior.STATE_EXPANDED)
            } else if (bottomSheetBehavior?.state == BottomSheetBehavior.STATE_EXPANDED) {
                bottomSheetBehavior?.state = BottomSheetBehavior.STATE_COLLAPSED
            }
        }

        tv_cancel_ccl.setOnClickListener {
            if (bottomSheetBehavior?.state == BottomSheetBehavior.STATE_EXPANDED) {
                bottomSheetBehavior?.state = BottomSheetBehavior.STATE_COLLAPSED
            }
        }

        bottomSheetBehavior?.setBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
            override fun onSlide(p0: View, p1: Float) {
            }

            override fun onStateChanged(bottomSheet: View, newState: Int) {
                when (newState) {
                    BottomSheetBehavior.STATE_DRAGGING -> com.khmer.com.utils.AppUtil.showLog("BottomSheetCallback", "BottomSheetBehavior.STATE_DRAGGING")
                    BottomSheetBehavior.STATE_SETTLING -> com.khmer.com.utils.AppUtil.showLog("BottomSheetCallback", "BottomSheetBehavior.STATE_SETTLING")
                    BottomSheetBehavior.STATE_EXPANDED -> com.khmer.com.utils.AppUtil.showLog("BottomSheetCallback", "BottomSheetBehavior.STATE_EXPANDED")
                    BottomSheetBehavior.STATE_COLLAPSED -> fake_bg.gone()
                    BottomSheetBehavior.STATE_HIDDEN -> com.khmer.com.utils.AppUtil.showLog("BottomSheetCallback", "BottomSheetBehavior.STATE_HIDDEN")
                }
            }
        })

        tv_en_ccl.setOnClickListener {
            AppSharePreference.putString(LANGUAGE, ENGLISH)
            iv_flag.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.flag_us))
            if (bottomSheetBehavior?.state == BottomSheetBehavior.STATE_EXPANDED) {
                bottomSheetBehavior?.state = BottomSheetBehavior.STATE_COLLAPSED
            }
            infoViewPost.showLoading()
            refreshData()
        }

        tv_km_ccl.setOnClickListener {
            AppSharePreference.putString(LANGUAGE, KHMER)
            iv_flag.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.flag_cam))
            if (bottomSheetBehavior?.state == BottomSheetBehavior.STATE_EXPANDED) {
                bottomSheetBehavior?.state = BottomSheetBehavior.STATE_COLLAPSED
            }
            infoViewPost.showLoading()
            refreshData()
        }

        tv_ja_ccl.setOnClickListener {
            AppSharePreference.putString(LANGUAGE, JAPANESE)
            iv_flag.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.flag_ja))
            if (bottomSheetBehavior?.state == BottomSheetBehavior.STATE_EXPANDED) {
                bottomSheetBehavior?.state = BottomSheetBehavior.STATE_COLLAPSED
            }
            infoViewPost.showLoading()
            refreshData()
        }

        tv_zh_ccl.setOnClickListener {
            AppSharePreference.putString(LANGUAGE, CHINESE)
            iv_flag.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.flag_zh))
            if (bottomSheetBehavior?.state == BottomSheetBehavior.STATE_EXPANDED) {
                bottomSheetBehavior?.state = BottomSheetBehavior.STATE_COLLAPSED
            }
            infoViewPost.showLoading()
            refreshData()
        }

        tv_ko_ccl.setOnClickListener {
            AppSharePreference.putString(LANGUAGE, KOREAN)
            iv_flag.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.flag_ko))
            if (bottomSheetBehavior?.state == BottomSheetBehavior.STATE_EXPANDED) {
                bottomSheetBehavior?.state = BottomSheetBehavior.STATE_COLLAPSED
            }
            infoViewPost.showLoading()
            refreshData()
        }

        tv_th_ccl.setOnClickListener {
            AppSharePreference.putString(LANGUAGE, THAI)
            iv_flag.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.flag_th))
            if (bottomSheetBehavior?.state == BottomSheetBehavior.STATE_EXPANDED) {
                bottomSheetBehavior?.state = BottomSheetBehavior.STATE_COLLAPSED
            }
            infoViewPost.showLoading()
            refreshData()
        }

        swipe_container.setOnRefreshListener {
            refreshData()
        }
    }

    private fun refreshData() {
        page = 1
        getPost(page)
    }

    private fun gotoEditActivity() {
        val intent = Intent(this, EditActivity::class.java)
        startActivityForResult(intent, UPDATE_PROFILE)
        overridePendingTransition(R.anim.enter, R.anim.hold)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == UPDATE_PROFILE && resultCode == Activity.RESULT_OK) {
            val username = AppSharePreference.getString(USERNAME, "")
            headerLayout.tv_username.text = username
        }
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        when (item.itemId) {
            R.id.action_settings -> return true
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.nav_faq -> {
                gotoWeb(URL_FAQ)
            }
            R.id.nav_anti_scam -> {
                gotoWeb(URL_ANTI_SCAM)
            }
            R.id.nav_term -> {
                gotoWeb(URL_TERM)
            }
            R.id.nav_privacy -> {
                gotoWeb(URL_PRIVACY)
            }
            R.id.nav_contact_us -> {
                gotoWeb(URL_CONTACT)
            }
        }

        drawer_layout.closeDrawer(GravityCompat.START)

        //return false to disable clicked shadow
        return false
    }

    private fun gotoWeb(url: String) {
        Handler().postDelayed({
            val intent = Intent(this, WebActivity::class.java)
            intent.putExtra(URL, url)
            startActivity(intent)
            overridePendingTransition(R.anim.enter, R.anim.hold)
        }, 500)
    }

    private val onResponseListener = object : OnResponseListener {
        override fun onResponded(isSuccess: Boolean) {
            swipe_container.isRefreshing = false
            infoViewPost.hide()
            if (isSuccess) {
                mPostListing = postManager.getPost()
                isLoadData = false
                if (mPostListing.data.isNotEmpty()) {
                    layout_no_data.invisible()
                    bindData(mPostListing)
                } else {
                    layout_no_data.visible()
                }
                isLoadData = false
            } else {
                layout_no_data.visible()
            }
        }

    }

    private fun bindData(post: Post) {
        if (page == 1) {
            mDataListing = post.data.toMutableList()
            layoutManager = LinearLayoutManager(this)
            layoutManager.orientation = LinearLayoutManager.VERTICAL
            postAdapter = PostAdapter(this, mDataListing)
            rv_post.layoutManager = layoutManager
            rv_post.setHasFixedSize(true)
            rv_post.addOnScrollListener(onScrollListener)
            rv_post.adapter = postAdapter
            if (page == post.totalPages?.toInt()) {
                postAdapter.noMoreData = true
                postAdapter.removeProgressBar()
            }
            page++
        } else {
            mDataListing.addAll(post.data.toMutableList())
            postAdapter.notifyDataSetChanged()
            if (page == post.totalPages?.toInt()) {
                postAdapter.noMoreData = true
                postAdapter.removeProgressBar()
            }
            page++
        }
    }

    private val onScrollListener = object : RecyclerView.OnScrollListener() {
        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            super.onScrolled(recyclerView, dx, dy)
            if (!isLoadData) {
                if (layoutManager.findFirstVisibleItemPosition() + recyclerView.childCount == layoutManager.itemCount && mDataListing.size != 0) {
                    if (page <= mPostListing.totalPages!!.toInt()) {
                        if (oldPage != page) {
                            getPost(page)
                        }
                    }
                }
            }
        }
    }


    private fun gotoLogin() {
        val intent = Intent(this, LoginActivity::class.java)
        startActivity(intent)
        finish()
        overridePendingTransition(R.anim.enter, R.anim.hold)
    }
}
