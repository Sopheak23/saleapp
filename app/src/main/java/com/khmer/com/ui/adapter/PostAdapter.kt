package com.khmer.com.ui.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.khmer.com.R
import com.khmer.com.model.DataItem
import com.khmer.com.ui.viewholder.NoDataViewHolder
import com.khmer.com.ui.viewholder.PostViewHolder
import com.khmer.com.ui.viewholder.ProgressBarViewHolder

class PostAdapter(private val context: Context, private val itemList: MutableList<DataItem>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var noMoreData = false
    private var mProgressBarPosition: Int = 0
    private var mNoDataPosition: Int = 0
    private var size: Int = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            R.layout.cell_post -> PostViewHolder.create(parent)
            R.layout.cell_progress_bar -> ProgressBarViewHolder.create(parent)
            R.layout.cell_no_data -> NoDataViewHolder.create(parent)
            else -> throw IllegalArgumentException("unknown view type")
        }
    }

    override fun getItemCount(): Int {
        size = itemList.size
        if (noMoreData) {
            size += 1
            mNoDataPosition = size - 1
            return size
        } else {
            size += 1
            mProgressBarPosition = size - 1
        }
        return size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder.itemViewType) {
            R.layout.cell_post -> (holder as PostViewHolder).bindTo(itemList[position], Glide.with(context))
            R.layout.cell_progress_bar -> (holder as ProgressBarViewHolder)
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (noMoreData) {
            if (position == mNoDataPosition) {
                R.layout.cell_no_data
            } else {
                R.layout.cell_post
            }
        } else {
            if (position == mProgressBarPosition) {
                R.layout.cell_progress_bar
            } else {
                R.layout.cell_post
            }
        }
    }

    fun removeProgressBar() {
        size -= 1
        notifyItemRemoved(size)
        notifyDataSetChanged()
    }
}