package com.sophal.homefinder.di.component

import com.sophal.homefinder.di.module.AppModule
import com.khmer.com.ui.*
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class])
interface AppComponent {
    /**Activity**/
    fun inject(signUpActivity: SignUpActivity)

    fun inject(editActivity: EditActivity)

    fun inject(editInfoActivity: EditInfoActivity)

    fun inject(loginActivity: LoginActivity)

    fun inject(mainActivity: MainActivity)

    /**Fragment**/

}