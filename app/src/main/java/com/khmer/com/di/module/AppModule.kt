package com.sophal.homefinder.di.module

import android.app.Application
import com.khmer.com.data.datamanager.AccountManager
import com.khmer.com.data.datamanager.PostManager
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule(private val application: Application) {

    @Singleton
    @Provides
    fun getAccountManager(): AccountManager {
        return AccountManager()
    }

    @Singleton
    @Provides
    fun getPostManager(): PostManager {
        return PostManager()
    }
}