package com.khmer.com.data.datamanager

import com.khmer.com.model.Profile
import com.khmer.com.model.User
import com.khmer.com.network.ApiHelper
import com.khmer.com.network.service.AccountService
import com.khmer.com.utils.OnResponseListener
import okhttp3.ResponseBody
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import javax.inject.Inject

class AccountManager @Inject constructor() {

    private var errorMessage: String = ""
    private var message: String = ""
    private lateinit var user: User

    fun registerUser(name: String, email: String, password: String, base64Image: String, onResponseListener: OnResponseListener) {
        ApiHelper.newInstance.create(AccountService::class.java).register(name, email, password, base64Image).enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.body() != null) {
                    try {
                        val str = response.body()!!.string()
                        val jsonObject = JSONObject(str)

                        if (jsonObject.optString("status") == "1") {
                            if (jsonObject.has("messages")) {
                                errorMessage = "Invalid input"
                                onResponseListener.onResponded(false)
                            } else {
                                user = parseUser(jsonObject)
                                onResponseListener.onResponded(true)
                            }
                        } else {
                            if (jsonObject.has("error")) {
                                errorMessage = jsonObject.optString("messages")
                            }
                            onResponseListener.onResponded(false)

                        }
                    } catch (e: IOException) {
                        e.printStackTrace()
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                } else {
                    onResponseListener.onResponded(false)
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                onResponseListener.onResponded(false)
            }
        })
    }

    fun login(email: String, password: String, onResponseListener: OnResponseListener) {
        ApiHelper.newInstance.create(AccountService::class.java).login(email, password).enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.body() != null) {
                    try {
                        val str = response.body()!!.string()
                        val jsonObject = JSONObject(str)

                        if (jsonObject.optString("status") == "1") {
                            if (jsonObject.has("messages")) {
                                errorMessage = jsonObject.optString("messages")
                                onResponseListener.onResponded(false)
                            } else {
                                user = parseUser(jsonObject)
                                onResponseListener.onResponded(true)
                            }
                        } else {
                            if (jsonObject.has("error")) {
                                errorMessage = jsonObject.optString("messages")
                            }
                            onResponseListener.onResponded(false)

                        }
                    } catch (e: IOException) {
                        e.printStackTrace()
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                } else {
                    val str = response.errorBody()!!.string()
                    val jsonObject = JSONObject(str)
                    if (jsonObject.optString("status") == "1") {
                        if (jsonObject.has("messages")) {
                            errorMessage = jsonObject.optString("messages")

                        }
                        onResponseListener.onResponded(false)
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                onResponseListener.onResponded(false)
            }
        })
    }

    fun logout(token: String, onResponseListener: OnResponseListener) {
        ApiHelper.newInstance.create(AccountService::class.java).logout(token).enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.body() != null) {
                    try {
                        val str = response.body()!!.string()
                        val jsonObject = JSONObject(str)

                        if (jsonObject.optString("status") == "1") {
                            if (jsonObject.has("messages")) {
                                message = jsonObject.optString("messages")
                            }
                            onResponseListener.onResponded(true)
                        } else {
                            if (jsonObject.has("error")) {
                                errorMessage = jsonObject.optString("messages")
                            }
                            onResponseListener.onResponded(false)

                        }
                    } catch (e: IOException) {
                        e.printStackTrace()
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                } else {
                    onResponseListener.onResponded(false)
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                onResponseListener.onResponded(false)
            }
        })
    }


    private fun parseUser(jsonObject: JSONObject): User {
        val user = User()
        val profile = Profile()
        val profileObject = jsonObject.optJSONObject("profile")
        profile.fileName = profileObject.optString("file_name")
        profile.thumbnail = profileObject.optString("thumbnail")
        user.id = jsonObject.optString("id")
        user.email = jsonObject.optString("email")
        user.name = jsonObject.optString("name")
        user.status = jsonObject.optString("status")
        user.token = jsonObject.optString("_token")
        user.profile = profile
        return user
    }


    fun getUser(): User {
        return user
    }

    fun getErrorMessage(): String {
        return errorMessage
    }

    fun getMessage(): String {
        return message
    }
}