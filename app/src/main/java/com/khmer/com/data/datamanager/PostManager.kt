package com.khmer.com.data.datamanager

import com.khmer.com.model.DataItem
import com.khmer.com.model.ImagesItem
import com.khmer.com.model.Post
import com.khmer.com.model.TranslateItem
import com.khmer.com.network.ApiHelper
import com.khmer.com.network.service.PostService
import com.khmer.com.utils.OnResponseListener
import okhttp3.ResponseBody
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import javax.inject.Inject

class PostManager @Inject constructor() {

    private var errorMessage: String = ""
    private lateinit var post: Post

    fun getPost(token: String, page: String, limit: String, onResponseListener: OnResponseListener) {
        ApiHelper.newInstance.create(PostService::class.java).getPost(token, page, limit).enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.body() != null) {
                    try {
                        val str = response.body()!!.string()
                        val jsonObject = JSONObject(str)

                        if (jsonObject.optString("status") == "1") {
                            post = parsePost(jsonObject)
                            onResponseListener.onResponded(true)

                        } else {
                            if (jsonObject.has("error")) {
                                errorMessage = jsonObject.optString("messages")
                            }
                            onResponseListener.onResponded(false)

                        }
                    } catch (e: IOException) {
                        onResponseListener.onResponded(false)
                        e.printStackTrace()
                    } catch (e: JSONException) {
                        onResponseListener.onResponded(false)
                        e.printStackTrace()
                    }
                } else {
                    onResponseListener.onResponded(false)
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                onResponseListener.onResponded(false)
            }


        })
    }

    private fun parsePost(jsonObject: JSONObject): Post {
        val post = Post()
        val dataList: MutableList<DataItem> = mutableListOf()
        post.currentPage = jsonObject.optString("current_page")
        post.totalPages = jsonObject.optString("total_pages")
        val data = jsonObject.optJSONArray("data")
        for (i in 0..(data.length() - 1)) {
            dataList.add(parseDataItem(data.optJSONObject(i)))
        }
        post.data = dataList
        post.status = jsonObject.optString("status")
        return post
    }

    private fun parseDataItem(jsonObject: JSONObject): DataItem {
        val data = DataItem()
        val translateList: MutableList<TranslateItem> = mutableListOf()
        val imageList: MutableList<ImagesItem> = mutableListOf()
        data.id = jsonObject.optString("id")
        data.companyName = jsonObject.optString("company_name")
        data.companyLogo = jsonObject.optString("company_logo")
        val translate = jsonObject.optJSONArray("translate")
        for (i in 0..(translate.length() - 1)) {
            translateList.add(parseTranslate(translate.optJSONObject(i)))
        }
        data.translate = translateList
        val images = jsonObject.optJSONArray("images")
        for (i in 0..(images.length() - 1)) {
            imageList.add(parseImage(images.optJSONObject(i)))
        }
        data.images = imageList
        data.createdAt = jsonObject.optString("created_at")
        return data
    }

    private fun parseTranslate(jsonObject: JSONObject): TranslateItem {
        val translate = TranslateItem()
        translate.language = jsonObject.optString("language")
        translate.caption = jsonObject.optString("caption")
        translate.postDetail = jsonObject.optString("post_detail")
        return translate
    }

    private fun parseImage(jsonObject: JSONObject): ImagesItem {
        val image = ImagesItem()
        image.fileName = jsonObject.optString("file_name")
        image.extension = jsonObject.optString("extension")
        image.thumbnail = jsonObject.optString("thumbnail")
        return image
    }

    fun getPost(): Post {
        return post
    }

    fun getErrorMessage(): String {
        return errorMessage
    }
}