package com.khmer.com.fragment


import android.os.Bundle
import android.support.v4.app.Fragment

import com.khmer.com.R
import com.khmer.com.base.BaseFragment

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class MainFragment : com.khmer.com.base.BaseFragment() {
    override fun layoutResId(): Int {
        return R.layout.fragment_main
    }

    override fun onInitView(savedInstanceState: Bundle?) {
        initGUI()
        initEvent()
    }

    private fun initGUI(){

    }

    private fun initEvent(){

    }

}
