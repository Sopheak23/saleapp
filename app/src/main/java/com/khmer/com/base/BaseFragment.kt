package com.khmer.com.base

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.khmer.com.utils.ViewLifeCycleFragment
import dagger.android.support.AndroidSupportInjection

abstract class BaseFragment : ViewLifeCycleFragment() {


    override fun onAttach(context: Context?) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        if (layoutResId() != 0) {
            val view = inflater.inflate(layoutResId(), container, false)
            return if (view != null) {
                view!!
            } else {
                inflater.inflate(layoutResId(), container, false)
            }
        }
        return super.onCreateView(inflater, container, savedInstanceState)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        onInitView(savedInstanceState)
    }


    override fun onDestroyView() {
        super.onDestroyView()

    }

    protected abstract fun layoutResId(): Int

    protected abstract fun onInitView(savedInstanceState: Bundle?)

}