package com.khmer.com.base

const val URL_FAQ: String = "https://khmerjob.com/page/faq"
const val URL_ANTI_SCAM: String = "https://khmerjob.com/page/anti-scam"
const val URL_TERM: String = "https://khmerjob.com/page/terms"
const val URL_PRIVACY: String = "https://khmerjob.com/page/privacy"
const val URL_CONTACT: String = "https://khmerjob.com/contact"

const val ENGLISH = "en-us"
const val KHMER = "km"
const val JAPANESE = "ja"
const val CHINESE = "zh"
const val KOREAN = "ko"
const val THAI = "th"

const val CONNECT_TIMEOUT = 10000
const val REQUEST_APP_PERMISSIONS = 0x1
const val UPDATE_PROFILE = 0x2
const val UPDATE_USERNAME = 0x3
const val BEARER = "Bearer "
const val TOKEN = "token"
const val IS_LOGIN = "isLogin"
const val IS_SKIP = "isSkip"
const val THUMBNAIL = "thumbnail"
const val USERNAME = "username"
const val EMAIL = "email"
const val APP_PREF_FILE_NAME = "KJ_pref"
const val URL = "url"
const val LANGUAGE = "language"