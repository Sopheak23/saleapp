package com.khmer.com.base

import android.app.Activity
import android.arch.lifecycle.LifecycleObserver
import android.arch.lifecycle.ViewModel
import android.os.Bundle
import android.support.v7.app.AppCompatActivity

abstract class BaseActivity<VM : ViewModel> : AppCompatActivity() {

    private lateinit var mActivity: Activity
    protected var mViewModel: VM? = null

    protected abstract fun layoutResId(): Int

    protected abstract fun onInitView(savedInstanceState: Bundle?)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layoutResId())
        onInitView(savedInstanceState)
        mActivity = this
    }

    override fun onDestroy() {
        super.onDestroy()
        if (mViewModel != null) {
            lifecycle.removeObserver(mViewModel as LifecycleObserver)
        }
    }

}